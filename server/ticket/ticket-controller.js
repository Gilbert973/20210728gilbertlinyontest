const baseController = require('../base/classes/controller-class');
const { isIDGood } = require('../base/config/mongo-utils');
const {
	checkQueryString,
	getItems,
	createItem,
	updateItem
} = require('../base/middleware/db');
const { handleError } = require('../base/utils/errorUtils');
const { checkModelProperties } = require('../base/utils/validator-utils');

const Ticket = require('./ticket-model');
const { findTicket } = require('./ticket-service');

class TicketController extends baseController {
	/**
	 * Get items function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async getTickets(req, res) {
		try {
			const query = await checkQueryString(req.query);
			res.status(200).json(await getItems(req, Ticket, query));
		} catch (error) {
			handleError(res, error);
		}
	}

	/**
	 * Get item function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async getTicket(req, res) {
		try {
			const _id = isIDGood(req.params.id);
			const item = await findTicket({ _id });
			res.status(200).json(item);
		} catch (error) {
			handleError(res, error);
		}
	}

	/**
	 * Create item function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async createTicket(req, res) {
		{
			try {
				const user = req.user;
				const ticket = await createItem(
					{ owner: isIDGood(user), ...req.body },
					Ticket
				);
				if (checkModelProperties(ticket, Ticket)) {
					Object.assign(
						ticket,
						await updateItem(isIDGood(ticket), Ticket, {
							isValid: true
						})
					);
				}
				res.status(201).json(ticket);
			} catch (error) {
				handleError(res, error);
			}
		}
	}

	/**
	 * Update item function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async updateTicket(req, res) {
		try {
			const id = isIDGood(req.params.id);
			res.status(200).json(await updateItem(id, Ticket, req.body));
		} catch (error) {
			handleError(res, error);
		}
	}
}

module.exports = new TicketController();
