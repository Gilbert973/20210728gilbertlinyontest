const trimRequest = require('trim-request');

require('../base/config/passport');
const passport = require('passport');
const requireAuth = passport.authenticate('jwt', {
	session: false
});

const TicketController = require('./ticket-controller');
const {
	checkTicketOwner
} = require('../base/validators/ticket-owner-validator');

const routes = {
	baseUri: '/ticket',
	GET: {
		// Get Tickets route
		'/': [
			requireAuth,
			TicketController.roleAuthorization(['admin']),
			trimRequest.all,
			TicketController.getTickets
		],
		// Get Ticket route
		'/:id': [
			requireAuth,
			TicketController.roleAuthorization(['user', 'admin']),
			checkTicketOwner,
			trimRequest.all,
			TicketController.getTicket
		]
	},
	POST: {
		// Create new Ticket route
		'/': [
			requireAuth,
			TicketController.roleAuthorization(['user', 'admin']),
			trimRequest.all,
			TicketController.createTicket
		]
	},
	PATCH: {
		// Update Ticket route
		'/:id': [
			requireAuth,
			TicketController.roleAuthorization(['user', 'admin']),
			trimRequest.all,
			checkTicketOwner,
			TicketController.updateTicket
		]
	},
	DELETE: {}
};

module.exports = routes;
