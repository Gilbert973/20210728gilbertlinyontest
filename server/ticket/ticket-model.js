const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const TicketSchema = new mongoose.Schema(
	{
		owner: { type: mongoose.Types.ObjectId, ref: 'User', required: true },
		title: String,
		description: String,
		status: {
			type: String,
			enum: ['open', 'closed', 'in progress', 'blocked'],
			default: 'open'
		},
		subject: {
			type: String,
			enum: ['technical', 'commercial', 'improvment'],
			default: 'technical'
		},
		priority: {
			type: String,
			enum: ['strong', 'medium', 'weak'],
			default: 'weak'
		}
	},
	{ versionKey: false, timestamps: true }
);

TicketSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Ticket', TicketSchema);
