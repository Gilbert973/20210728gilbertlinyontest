const { itemNotFound } = require('../base/utils/errorUtils');
const ticketModel = require('./ticket-model');

module.exports = { findTicket };

function findTicket(fields = {}, config = '') {
	return new Promise((resolve, reject) => {
		ticketModel.findOne(fields, config, async (err, item) => {
			try {
				await itemNotFound(err, item, 'TICKET_DOES_NOT_EXIST');
				resolve(item);
			} catch (error) {
				reject(error);
			}
		});
	});
}
