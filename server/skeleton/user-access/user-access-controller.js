const baseController = require('../../base/classes/controller-class');

class UserAccessController extends baseController {}

module.exports = new UserAccessController();
