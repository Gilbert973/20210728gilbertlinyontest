const trimRequest = require('trim-request');

require('../../base/config/passport');
const passport = require('passport');
const requireAuth = passport.authenticate('jwt', {
	session: false
});

const UserAccessController = require('./user-access-controller');
const {
	validateUpdateUserAccess
} = require('../../base/validators/user-access-validator');
const userAccessModel = require('./user-access-model');

const routes = {
	baseUri: '/user/access/',
	GET: {},
	POST: {},
	PATCH: {
		'display/:id': [
			requireAuth,
			UserAccessController.roleAuthorization(['user', 'admin']),
			trimRequest.all,
			validateUpdateUserAccess,
			UserAccessController.display(userAccessModel)
		]
	},
	// Delete User route
	DELETE: {}
};

module.exports = routes;
