const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const UserAccessSchema = new mongoose.Schema(
	{
		user: { type: mongoose.Types.ObjectId, ref: 'User' },
		ip: { type: String, required: true },
		browser: { type: String, required: true },
		country: { type: String, required: true },
		display: { type: Boolean, default: true }
	},
	{ versionKey: false, timestamps: true }
);

UserAccessSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('UserAccess', UserAccessSchema);
