const trimRequest = require('trim-request');

const {
	validateRegister,
	validateLogin,
	validateVerify,
	validateCreateUser,
	validateGetUser,
	validateUpdateUser,
	validateDeleteUser
} = require('../../base/validators/user-validator');

require('../../base/config/passport');
const passport = require('passport');
const requireAuth = passport.authenticate('jwt', {
	session: false
});

const UserController = require('./user-controller');

const routes = {
	baseUri: '',
	GET: {
		// Get Users route
		'/user': [
			requireAuth,
			UserController.roleAuthorization(['admin']),
			trimRequest.all,
			UserController.getUsers
		],
		// Get User route
		'/user/:id': [
			requireAuth,
			UserController.roleAuthorization(['user', 'admin']),
			UserController.checkUserId,
			trimRequest.all,
			validateGetUser,
			UserController.getUser
		],
		// Get new refresh token
		'/token': [
			requireAuth,
			UserController.roleAuthorization(['user', 'admin']),
			trimRequest.all,
			UserController.getRefreshToken
		],
		'/user/activities/:id': [
			requireAuth,
			UserController.roleAuthorization(['user', 'admin']),
			UserController.checkUserId,
			trimRequest.all,
			UserController.getActivities
		]
	},
	POST: {
		// Register route
		'/register': [
			trimRequest.all,
			validateRegister,
			UserController.register
		],
		// Create new User route
		'/user': [
			requireAuth,
			UserController.roleAuthorization(['admin']),
			trimRequest.all,
			validateCreateUser,
			UserController.createUser
		],
		// Login route
		'/login': [
			// requireAuth,
			trimRequest.all,
			validateLogin,
			UserController.login
		]
	},
	PATCH: {
		//  Verify a User
		'/user/verify': [
			// requireAuth,
			trimRequest.all,
			validateVerify,
			UserController.verify
		],
		'/user/:id': [
			requireAuth,
			UserController.roleAuthorization(['admin', 'user']),
			UserController.checkUserId,
			trimRequest.all,
			validateUpdateUser,
			UserController.updateUser
		]
	},
	// Delete User route
	DELETE: {
		'/user/:id': [
			requireAuth,
			UserController.roleAuthorization(['admin']),
			trimRequest.all,
			validateDeleteUser,
			UserController.deleteUser
		]
	}
};

module.exports = routes;
