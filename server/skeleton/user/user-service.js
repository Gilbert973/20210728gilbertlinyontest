const {
	getIP,
	getBrowserInfo,
	getCountry,
	buildErrObject,
	itemNotFound,
	isset
} = require('../../base/utils');

const { getItems } = require('../../base/middleware/db');
const { encrypt, decrypt } = require('../../base/middleware/user');
const { isIDGood } = require('../../base/config/mongo-utils');

const { addHours } = require('date-fns');
const User = require('./user-model');
const jwt = require('jsonwebtoken');
const uuid = require('uuid');

const userAccessModel = require('../user-access/user-access-model');
const userForgotPasswordModel = require('../user-forgot-password/user-forgot-password-model');

module.exports = {
	blockIsExpired,
	blockUser,
	checkLoginAttemptsAndBlockExpires,
	findUser,
	findUserById,
	generateToken,
	getUserIdFromToken,
	registerUser,
	returnRegisterToken,
	saveLoginAttemptsToDB,
	updatePassword,
	userIsBlocked,
	verificationExists,
	setUserInfo,
	saveUserAccessAndReturnToken,
	verifyUser,
	createUserInDb,
	getActivities
};

const LOGIN_ATTEMPTS = 5;
/**
 * Checks that login attempts are greater than specified in constant and also that blockexpires is less than now
 * @param {Object} user - user object
 */
function blockIsExpired({ loginAttempts = 0, blockExpires = '' }) {
	return loginAttempts > LOGIN_ATTEMPTS && blockExpires <= new Date();
}

const HOURS_TO_BLOCK = 2;
/**
 * Blocks a user by setting blockExpires to the specified date based on constant HOURS_TO_BLOCK
 * @param {Object} user - user object
 */
function blockUser(user = {}) {
	return new Promise((resolve, reject) => {
		user.blockExpires = addHours(new Date(), HOURS_TO_BLOCK);
		user.save((err, result) => {
			if (err) {
				return reject(buildErrObject(422, err.message));
			}
			if (result) {
				return resolve(buildErrObject(409, 'BLOCKED_USER'));
			}
		});
	});
}

/**
 *
 * @param {Object} user - user object.
 */
function checkLoginAttemptsAndBlockExpires(user = {}) {
	return new Promise((resolve, reject) => {
		// Let user try to login again after blockexpires, resets user loginAttempts
		if (blockIsExpired(user)) {
			user.loginAttempts = 0;
			user.save((err, result) => {
				if (err) {
					return reject(buildErrObject(422, err.message));
				}
				if (result) {
					return resolve(true);
				}
			});
		}
		// User is not blocked, check password (normal behaviour)
		resolve(true);
	});
}

/**
 * Finds user by email
 * @param {string} email - user´s email
 */
function findUser(
	email = '',
	config = '_id password loginAttempts blockExpires name email role verified verification'
) {
	return new Promise((resolve, reject) => {
		User.findOne({ email }, config, async (err, item) => {
			try {
				await itemNotFound(err, item, 'USER_DOES_NOT_EXIST');
				resolve(item);
			} catch (error) {
				reject(error);
			}
		});
	});
}

/**
 * Finds user by ID
 * @param {string} id - user´s id
 */
function findUserById(userId = '', config = '') {
	return new Promise((resolve, reject) => {
		User.findById(userId, config, async (err, item) => {
			try {
				await itemNotFound(err, item, 'USER_DOES_NOT_EXIST');
				resolve(item);
			} catch (error) {
				reject(error);
			}
		});
	});
}

/**
 * Generates a token
 * @param {Object} user - user object
 */
function generateToken(user = '') {
	try {
		// Gets expiration time
		const expiration =
			Math.floor(Date.now() / 1000) +
			60 * process.env.JWT_EXPIRATION_IN_MINUTES;

		// returns signed and encrypted token
		return encrypt(
			jwt.sign(
				{
					data: { _id: user },
					exp: expiration
				},
				process.env.JWT_SECRET
			)
		);
	} catch (error) {
		throw error;
	}
}

/**
 * Gets user id from token
 * @param {string} token - Encrypted and encoded token
 */
function getUserIdFromToken(token = '') {
	return new Promise((resolve, reject) => {
		// Decrypts, verifies and decode token
		jwt.verify(decrypt(token), process.env.JWT_SECRET, (err, decoded) => {
			if (err) {
				reject(buildErrObject(409, 'BAD_TOKEN'));
			}
			resolve(isIDGood(decoded.data._id));
		});
	});
}

/**
 * Registers a new user in database
 * @param {Object} req - request object
 */
function registerUser(req = {}) {
	return new Promise((resolve, reject) => {
		const user = new User({
			...req,
			verification: uuid.v4()
		});
		user.save((err, item) => {
			if (err) {
				reject(buildErrObject(422, err.message));
			}
			resolve(item);
		});
	});
}

/**
 * Builds the registration token
 * @param {Object} item - user object that contains created id
 * @param {Object} userInfo - user object
 */
function returnRegisterToken({ _id = '', verification = '' }, userInfo = {}) {
	return new Promise(resolve => {
		if (process.env.NODE_ENV !== 'production') {
			userInfo.verification = verification;
		}
		const data = {
			token: generateToken(_id),
			user: userInfo
		};
		resolve(data);
	});
}

/**
 * Saves login attempts to dabatabse
 * @param {Object} user - user object
 */
function saveLoginAttemptsToDB(user = {}) {
	return new Promise((resolve, reject) => {
		user.save((err, result) => {
			if (err) {
				return reject(buildErrObject(422, err.message));
			}
			if (result) {
				resolve(true);
			}
		});
	});
}

/**
 * Updates a user password in database
 * @param {string} password - new password
 * @param {Object} user - user object
 */
function updatePassword(password = '', user = {}) {
	return new Promise((resolve, reject) => {
		user.password = password;
		user.save(async (err, item) => {
			try {
				await itemNotFound(err, item, 'NOT_FOUND');
				resolve(item);
			} catch (error) {
				reject(error);
			}
		});
	});
}

/**
 * Checks if blockExpires from user is greater than now
 * @param {Object} user - user object
 */
function userIsBlocked(user = {}) {
	return new Promise((resolve, reject) => {
		if (user.blockExpires > new Date()) {
			return reject(buildErrObject(409, 'BLOCKED_USER'));
		}
		resolve(true);
	});
}

/**
 * Checks if verification id exists for user
 * @param {string} id - verification id
 */
function verificationExists(id = '') {
	return new Promise((resolve, reject) => {
		User.findOne(
			{
				verification: id,
				verified: false
			},
			async (err, user) => {
				try {
					await itemNotFound(
						err,
						user,
						'NOT_FOUND_OR_ALREADY_VERIFIED'
					);
					resolve(user);
				} catch (error) {
					reject(error);
				}
			}
		);
	});
}

function setUserInfo(req = {}) {
	return new Promise(resolve => {
		let user = {
			_id: req._id,
			// firstName: req.firstName,
			// lastName: req.lastName,
			email: req.email,
			role: req.role,
			verified: req.verified
		};
		// Adds verification for testing purposes
		if (process.env.NODE_ENV !== 'production') {
			user = {
				...user,
				verification: req.verification
			};
		}
		resolve(user);
	});
}

/**
 * Saves a new user access and then returns token
 * @param {Object} req - request object
 * @param {Object} user - user object
 */
function saveUserAccessAndReturnToken(req = {}, user = {}) {
	return new Promise((resolve, reject) => {
		const userAccess = new userAccessModel({
			user: isIDGood(user),
			ip: getIP(req),
			browser: getBrowserInfo(req),
			country: getCountry(req)
		});
		userAccess.save(async err => {
			try {
				if (err) {
					return reject(buildErrObject(422, err.message));
				}
				const userInfo = await setUserInfo(user);
				// Returns data with access token
				resolve({
					token: generateToken(isIDGood(user)),
					user: userInfo
				});
			} catch (error) {
				reject(error);
			}
		});
	});
}

/**
 * Verifies an user
 * @param {Object} user - user object
 */
function verifyUser(user = {}) {
	return new Promise((resolve, reject) => {
		user.verified = true;
		user.save((err, item) => {
			if (err) {
				return reject(buildErrObject(422, err.message));
			}
			resolve({
				email: item.email,
				verified: item.verified
			});
		});
	});
}

/**
 * Creates a new item in database
 * @param {Object} user - request object
 */
function createUserInDb(user = {}) {
	return new Promise((resolve, reject) => {
		const createdUser = new User({ ...user, verification: uuid.v4() });
		createdUser.save((err, item) => {
			if (err) reject(buildErrObject(422, err.message));

			item = JSON.parse(JSON.stringify(item));
			delete item.password;
			delete item.blockExpires;
			delete item.loginAttempts;

			resolve(item);
		});
	});
}

/**
 * @param {Object} req - request object
 * @param {Object} query - request object
 */
async function getActivities(req, query = {}) {
	const userAccesses = await getItems(req, userAccessModel, query, true);
	let results = [];
	if (isset(userAccesses) && isset(userAccesses.docs)) {
		results = results.concat(
			userAccesses.docs.map(item => ({
				id: item._id,
				type: 'UserAccess',
				user: item.user,
				ip: item.ip,
				browser: item.browser,
				country: item.country,
				display: item.display
			}))
		);
	}

	const userForgotPasswords = await getItems(
		req,
		userForgotPasswordModel,
		query,
		true
	);
	if (isset(userForgotPasswords) && isset(userForgotPasswords.docs)) {
		results = results.concat(
			userForgotPasswords.docs.map(item => ({
				type: 'ForgotPassword',
				user: item.user,
				ip: item.ipRequest,
				browser: item.browserRequest,
				country: item.countryRequest
			}))
		);
	}
	return results;
}
