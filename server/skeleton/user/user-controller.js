const { matchedData } = require('express-validator');
const {
	registerUser,
	setUserInfo,
	returnRegisterToken,
	userIsBlocked,
	findUser,
	checkLoginAttemptsAndBlockExpires,
	saveLoginAttemptsToDB,
	saveUserAccessAndReturnToken,
	getUserIdFromToken,
	findUserById,
	verificationExists,
	verifyUser,
	saveForgotPassword,
	forgotPasswordResponse,
	createUserInDb,
	getActivities
} = require('./user-service');
const {
	emailExists,
	sendRegistrationEmailMessage,
	sendResetPasswordEmailMessage,
	emailExistsExcludingMyself
} = require('../../base/middleware/emailer');
const { handleError } = require('../../base/utils');
const { checkPassword } = require('../../base/middleware/user');
const baseController = require('../../base/classes/controller-class');
const { isIDGood } = require('../../base/config/mongo-utils');
const {
	checkQueryString,
	getItems,
	getItem,
	updateItem,
	deleteItem
} = require('../../base/middleware/db');
const User = require('./user-model');
const { passwordsDoNotMatch } = require('../../base/validators/user-validator');

class UserController extends baseController {
	/**
	 * Register function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async register(req, res) {
		try {
			// Gets locale from header 'Accept-Language'
			const locale = req.getLocale();
			req = matchedData(req);
			const doesEmailExists = await emailExists(req.email);
			if (!doesEmailExists) {
				const item = await registerUser(req);
				const userInfo = await setUserInfo(item);
				const response = await returnRegisterToken(item, userInfo);
				sendRegistrationEmailMessage(locale, item);
				res.status(201).json(response);
			}
		} catch (error) {
			handleError(res, error);
		}
	}

	/**
	 * Login function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async login(req, res) {
		try {
			const data = matchedData(req);
			const user = await findUser(data.email);
			await userIsBlocked(user);
			await checkLoginAttemptsAndBlockExpires(user);
			const isPasswordMatch = await checkPassword(data.password, user);
			if (!isPasswordMatch) {
				handleError(res, await passwordsDoNotMatch(user));
			} else {
				// all ok, register access and return token
				user.loginAttempts = 0;
				await saveLoginAttemptsToDB(user);
				res.status(200).json(
					await saveUserAccessAndReturnToken(req, user)
				);
			}
		} catch (error) {
			handleError(res, error);
		}
	}

	/**
	 * Refresh token function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async getRefreshToken(req, res) {
		try {
			const tokenEncrypted = req.headers.authorization
				.replace('Bearer ', '')
				.trim();
			const userId = await getUserIdFromToken(tokenEncrypted);
			const user = await findUserById(userId);
			const token = await saveUserAccessAndReturnToken(req, user);
			// Removes user info from response
			delete token.user;
			res.status(200).json(token);
		} catch (error) {
			handleError(res, error);
		}
	}

	/**
	 * Verify function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async verify(req, res) {
		try {
			req = matchedData(req);
			const user = await verificationExists(req.id);
			res.status(200).json(await verifyUser(user));
		} catch (error) {
			handleError(res, error);
		}
	}

	/**
	 * Forgot password function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async forgotPassword(req, res) {
		try {
			// Gets locale from header 'Accept-Language'
			const locale = req.getLocale();
			const data = matchedData(req);
			const user = await findUser(data.email);
			const item = await saveForgotPassword(req, user);
			sendResetPasswordEmailMessage(locale, item);
			res.status(200).json(forgotPasswordResponse(item));
		} catch (error) {
			handleError(res, error);
		}
	}

	/**
	 * Get items function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async getUsers(req, res) {
		try {
			const query = await checkQueryString(req.query);
			res.status(200).json(await getItems(req, User, query));
		} catch (error) {
			handleError(res, error);
		}
	}

	/**
	 * Create item function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async createUser(req, res) {
		{
			try {
				// Gets locale from header 'Accept-Language'
				const locale = req.getLocale();
				req = matchedData(req);
				const doesEmailExists = await emailExists(req.email);
				if (!doesEmailExists) {
					const item = await createUserInDb(req);
					sendRegistrationEmailMessage(locale, item);
					res.status(201).json(item);
				}
			} catch (error) {
				handleError(res, error);
			}
		}
	}

	/**
	 * Get item function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async getUser(req, res) {
		try {
			req = matchedData(req);
			const id = isIDGood(req.id);
			res.status(200).json(await getItem(id, User));
		} catch (error) {
			handleError(res, error);
		}
	}

	/**
	 * Update item function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async updateUser(req, res) {
		try {
			req = matchedData(req);
			const id = isIDGood(req.id);
			const doesEmailExists = await emailExistsExcludingMyself(
				id,
				req.email
			);
			if (!doesEmailExists) {
				res.status(200).json(await updateItem(id, User, req));
			}
		} catch (error) {
			handleError(res, error);
		}
	}

	/**
	 * Delete item function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async deleteUser(req, res) {
		try {
			req = matchedData(req);
			const id = isIDGood(req.id);
			res.status(200).json(await deleteItem(id, User));
		} catch (error) {
			handleError(res, error);
		}
	}

	/**
	 * Get all User activities (UserAccess + UserForgotPassword)
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async getActivities(req, res) {
		try {
			const query = await checkQueryString(req.query);
			query.user = isIDGood(req.params.id);
			query.display = true;
			res.status(200).json(await getActivities(req, query));
		} catch (error) {
			handleError(res, error);
		}
	}
}

module.exports = new UserController();
