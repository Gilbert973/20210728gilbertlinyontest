const { matchedData } = require('express-validator');
const {
	saveForgotPassword,
	forgotPasswordResponse,
	findForgotPassword,
	updatePassword,
	markResetPasswordAsUsed
} = require('./user-forgot-password-service');

const { findUser, findUserById } = require('../user/user-service');

const {
	sendResetPasswordEmailMessage
} = require('../../base/middleware/emailer');
const { handleError, buildErrObject } = require('../../base/utils');
const baseController = require('../../base/classes/controller-class');
const { isIDGood } = require('../../base/config/mongo-utils');
const { checkPassword } = require('../../base/middleware/user');

class UserController extends baseController {
	/**
	 * Forgot password function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async forgotPassword(req, res) {
		try {
			// Gets locale from header 'Accept-Language'
			const locale = req.getLocale();
			const data = matchedData(req);
			const user = await findUser(data.email);
			const forgot = await saveForgotPassword(req, user);
			sendResetPasswordEmailMessage(locale, forgot);
			res.status(200).json(forgotPasswordResponse(forgot));
		} catch (error) {
			handleError(res, error);
		}
	}

	/**
	 * Reset password function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async resetPassword(req, res) {
		try {
			const data = matchedData(req);
			const forgotPassword = await findForgotPassword(data.id);
			const user = await findUserById(isIDGood(forgotPassword.user));
			await updatePassword(data.password, user);
			const result = await markResetPasswordAsUsed(req, forgotPassword);
			return res.status(200).json(result);
		} catch (error) {
			handleError(res, error);
		}
	}

	/**
	 * Change password function called by route
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	async changePassword(req, res) {
		try {
			const id = await isIDGood(req.user._id);
			const user = await findUserById(id, 'password');
			req = matchedData(req);
			const isPasswordMatch = await checkPassword(req.oldPassword, user);
			if (!isPasswordMatch) {
				return handleError(res, buildErrObject(409, 'WRONG_PASSWORD'));
			} else {
				// all ok, proceed to change password
				await updatePassword(req.newPassword, user);
				res.status(200).json({ _id: id });
			}
		} catch (error) {
			handleError(res, error);
		}
	}
}

module.exports = new UserController();
