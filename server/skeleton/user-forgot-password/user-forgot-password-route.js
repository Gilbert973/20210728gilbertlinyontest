const trimRequest = require('trim-request');

const {
	validateForgotPassword,
	validateResetPassword,
	validateChangePassword
} = require('../../base/validators/user-validator');

require('../../base/config/passport');
const passport = require('passport');
const requireAuth = passport.authenticate('jwt', {
	session: false
});

const UserForgotPasswordController = require('./user-forgot-password-controller');
const {
	validateUpdateUserAccess
} = require('../../base/validators/user-access-validator');
const userForgotPasswordModel = require('./user-forgot-password-model');

const routes = {
	baseUri: '/password',
	POST: {
		// Forgot password route
		'/forgot': [
			trimRequest.all,
			validateForgotPassword,
			UserForgotPasswordController.forgotPassword
		],
		// Reset password route
		'/reset': [
			trimRequest.all,
			validateResetPassword,
			UserForgotPasswordController.resetPassword
		]
	},
	PATCH: {
		// Change password route
		'/change': [
			requireAuth,
			UserForgotPasswordController.roleAuthorization(['user', 'admin']),
			trimRequest.all,
			validateChangePassword,
			UserForgotPasswordController.changePassword
		],
		'/display/:id': [
			requireAuth,
			UserForgotPasswordController.roleAuthorization(['user', 'admin']),
			trimRequest.all,
			validateUpdateUserAccess,
			UserForgotPasswordController.display(userForgotPasswordModel)
		]
	}
};

module.exports = routes;
