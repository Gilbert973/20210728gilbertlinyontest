const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const UserForgotPassword = new mongoose.Schema(
	{
		user: { type: mongoose.Types.ObjectId, ref: 'User' },
		verification: { type: String },
		used: { type: Boolean, default: false },
		ipRequest: { type: String },
		browserRequest: { type: String },
		countryRequest: { type: String },
		ipChanged: { type: String },
		browserChanged: { type: String },
		countryChanged: { type: String },
		display: { type: Boolean, default: true }
	},
	{ versionKey: false, timestamps: true }
);

UserForgotPassword.plugin(mongoosePaginate);

module.exports = mongoose.model('UserForgotPassword', UserForgotPassword);
