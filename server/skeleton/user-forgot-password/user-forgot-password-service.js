const {
	buildErrObject,
	itemNotFound,
	buildSuccObject,
	getIP,
	getBrowserInfo,
	getCountry
} = require('../../base/utils');
const uuid = require('uuid');

const UserForgotPasswordModel = require('../user-forgot-password/user-forgot-password-model');
const { isIDGood } = require('../../base/config/mongo-utils');

module.exports = {
	saveForgotPassword,
	forgotPasswordResponse,
	findForgotPassword,
	updatePassword,
	markResetPasswordAsUsed
};

/**
 * Creates a new password forgot
 * @param {Object} req - request object
 */
function saveForgotPassword(req = {}, user = {}) {
	return new Promise((resolve, reject) => {
		const forgot = new UserForgotPasswordModel({
			user: isIDGood(user),
			verification: uuid.v4(),
			ipRequest: getIP(req),
			browserRequest: getBrowserInfo(req),
			countryRequest: getCountry(req)
		});

		forgot.save().then((res, err) => {
			if (err) {
				return reject(buildErrObject(422, err.message));
			}
			UserForgotPasswordModel.populate(forgot, {
				path: 'user',
				select: 'email firstName lastName'
			}).then(item => resolve(item));
		});
	});
}

/**
 * Builds an object with created forgot password object, if env is development or testing exposes the verification
 * @param {Object} item - created forgot password object
 */
function forgotPasswordResponse(forgot = {}) {
	let data = {
		msg: 'RESET_EMAIL_SENT',
		email: forgot.user.email
	};
	if (process.env.NODE_ENV !== 'production') {
		data = {
			...data,
			verification: forgot.verification
		};
	}
	return data;
}

/**
 * Checks if a forgot password verification exists
 * @param {string} id - verification id
 */
function findForgotPassword(id = '') {
	return new Promise((resolve, reject) => {
		UserForgotPasswordModel.findOne(
			{
				verification: id,
				used: false
			},
			async (err, item) => {
				try {
					await itemNotFound(err, item, 'NOT_FOUND_OR_ALREADY_USED');
					resolve(item);
				} catch (error) {
					reject(error);
				}
			}
		);
	});
}

/**
 * Updates a user password in database
 * @param {string} password - new password
 * @param {Object} user - user object
 */
function updatePassword(password = '', user = {}) {
	return new Promise((resolve, reject) => {
		user.password = password;
		user.save(async (err, item) => {
			try {
				await itemNotFound(err, item, 'NOT_FOUND');
				resolve(item);
			} catch (error) {
				reject(error);
			}
		});
	});
}

/**
 * Marks a request to reset password as used
 * @param {Object} req - request object
 * @param {Object} forgot - forgot object
 */
function markResetPasswordAsUsed(req = {}, forgot = {}) {
	return new Promise((resolve, reject) => {
		forgot.used = true;
		forgot.ipChanged = getIP(req);
		forgot.browserChanged = getBrowserInfo(req);
		forgot.countryChanged = getCountry(req);
		return forgot.save(async (err, item) => {
			try {
				await itemNotFound(err, item, 'NOT_FOUND');
				resolve(buildSuccObject('PASSWORD_CHANGED'));
			} catch (error) {
				reject(error);
			}
		});
	});
}
