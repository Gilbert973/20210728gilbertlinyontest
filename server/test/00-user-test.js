/* eslint handle-callback-err: "off"*/

process.env.NODE_ENV = 'test';

const frisby = require('frisby');
const qs = require('qs');
const fs = require('fs');
// const uuid = require('uuid');

const { config, loginDetails, asserts } = require('../base/config/test-config');
const fullUrl = `${config.url}:${config.port}`;

const faker = require('faker');
const email = faker.internet.email();

let verification;
let verificationForgot;
const createdID = [];

let tokens;

frisby.globalSetup({
	request: {
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}
});

describe('********************** USER TEST **********************', () => {
	describe('POST - Admin & User login', () => {
		it('200 - Admin should get a token', () =>
			frisby
				.post(`${fullUrl}/login`, {
					body: qs.stringify(loginDetails.admin)
				})
				.expect('status', 200)
				.then(response => {
					const array = [];
					array.push(response._json);
					fs.writeFile(
						'../json_files/user_tokens.json',
						`${JSON.stringify(array)}`,
						err => {
							if (err) return console.log(err);
						}
					);
				}));

		it('200 - User should get a token', () =>
			frisby
				.post(`${fullUrl}/login`, {
					body: qs.stringify(loginDetails.user)
				})
				.expect('status', 200)
				.then(response => {
					const content = fs.readFileSync(
						'../json_files/user_tokens.json'
					);
					const array = JSON.parse(content);
					array.push(response._json);
					fs.writeFile(
						'../json_files/user_tokens.json',
						`${JSON.stringify(array)}`,
						err => {
							if (err) return console.log(err);
						}
					);
				}));
	});

	describe('POST - User registration tests', () => {
		it('201 - User should register', () => {
			const user = {
				firstName: faker.random.words(),
				lastName: faker.random.words(),
				email,
				password: 'qwertyuiop'
			};
			return (
				frisby
					.post(`${fullUrl}/register`, {
						body: qs.stringify(user)
					})
					.expect('status', 201)
					.expect('jsonTypesStrict', '', asserts.auth)
					// eslint-disable-next-line prefer-arrow-callback
					.then(function (response) {
						verification = response._json.user.verification;
						createdID.push(response._json.user._id);
						const content = fs.readFileSync(
							'../json_files/user_tokens.json'
						);
						const array = JSON.parse(content);
						array.push(response._json);
						fs.writeFile(
							'../json_files/user_tokens.json',
							`${JSON.stringify(array)}`,
							err => {
								if (err) return console.log(err);
							}
						);
					})
			);
		});

		it('Read tokens fron user_tokens.json file', done => {
			tokens = JSON.parse(
				fs.readFileSync('../json_files/user_tokens.json')
			);
			done();
		});

		it('422 - User should NOT register if email already exists', () => {
			const user = {
				firstName: faker.random.words(),
				lastName: faker.random.words(),
				email,
				password: 'qwertyuiop'
			};
			return frisby
				.post(`${fullUrl}/register`, {
					body: qs.stringify(user)
				})
				.expect('status', 422);
		});
	});

	describe('POST - User-s email verification ', () => {
		/* TODO it('200 - User should POST verify', () =>
			frisby
				.patch(`${fullUrl}/user/verify`, {
					body: qs.stringify({ id: uuid.v4() })
				})
				.expect('status', 422));*/

		it('200 - User should POST verify', () =>
			frisby
				.patch(`${fullUrl}/user/verify`, {
					body: qs.stringify({ id: verification })
				})
				.expect('status', 200)
				.expect('jsonTypesStrict', '', asserts.verify));
	});

	describe('PATCH - Admin & User could change their password', () => {
		it('it should change the ADMIN password', () =>
			frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[0].token}`
						}
					}
				})
				.patch(`${fullUrl}/password/change`, {
					body: qs.stringify({
						oldPassword: '12345',
						newPassword: '12345'
					})
				})
				.expect('status', 200));

		it('it should change the USER password', () =>
			frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[1].token}`
						}
					}
				})
				.patch(`${fullUrl}/password/change`, {
					body: qs.stringify({
						oldPassword: '12345',
						newPassword: '12345'
					})
				})
				.expect('status', 200));
	});

	describe('POST - User forgot his passowrd & reset', () => {
		it('User should POST forgot', () =>
			frisby
				.post(`${fullUrl}/password/forgot`, {
					body: qs.stringify({ email })
				})
				.expect('status', 200)
				.expect('jsonTypesStrict', '', asserts.forgotPwd)
				.then(response => {
					verificationForgot = response._json.verification;
				}));

		it('User should reset his password', () =>
			frisby
				.post(`${fullUrl}/password/reset`, {
					body: qs.stringify({
						id: verificationForgot,
						password: '12345'
					})
				})
				.expect('status', 200));
	});

	describe('GET - Only auth User could access to refresh token route', () => {
		it('User should NOT be able to consume the route since no token was sent', () =>
			frisby.get(`${fullUrl}/token`).expect('status', 401));
		it('it should GET a fresh token', () =>
			frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[1].token}`
						}
					}
				})
				.get(`${fullUrl}/token`)
				.expect('status', 200)
				.expect('jsonTypesStrict', '', asserts.refresh)
				.then(response => {
					tokens[1].token = response._json.token;
					fs.writeFile(
						'../json_files/user_tokens.json',
						`${JSON.stringify(tokens)}`,
						err => {
							if (err) return console.log(err);
						}
					);
				}));
	});

	describe('GET - User route', () => {
		it('401 - Client should NOT be able to consume the route since no token was sent', () =>
			frisby.get(`${fullUrl}/user`).expect('status', 401));
		it('401 - User cannot access to users list', () =>
			frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[1].token}`
						}
					}
				})
				.get(`${fullUrl}/user`)
				.expect('status', 401));
		it('200 - Admin should GET all the users', () =>
			frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[0].token}`
						}
					}
				})
				.get(`${fullUrl}/user`)
				.expect('status', 200)
				.expect('jsonTypes', '', asserts.docs));
		it('200 - Admin should GET all admins', () =>
			frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[0].token}`
						}
					}
				})
				.get(`${fullUrl}/user?filter=admin&fields=name,email,phone`)
				.expect('status', 200)
				.expect('jsonTypes', '', asserts.docs)
				.expect('jsonTypes', 'docs.*', [{ email: 'admin@admin.com' }]));
	});

	describe('POST - Create user in the app', () => {
		it('422 - Admin should NOT POST an empty user', () =>
			frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[0].token}`
						}
					}
				})
				.post(`${fullUrl}/user`, { body: {} })
				.expect('status', 422));

		it('201 - Admin should POST a user', () => {
			const user = {
				firstName: faker.random.words(),
				lastName: faker.random.words(),
				email: faker.internet.email(),
				password: 'qwertyuiop',
				role: 'admin',
				phone: faker.phone.phoneNumber()
			};
			return (
				frisby
					.setup({
						request: {
							headers: {
								Authorization: `Bearer ${tokens[0].token}`
							}
						}
					})
					.post(`${fullUrl}/user`, { body: qs.stringify(user) })
					.expect('status', 201)
					.expect('jsonTypes', '', asserts.user)
					// eslint-disable-next-line prefer-arrow-callback
					.then(function (response) {
						createdID.push(response._json._id);
					})
			);
		});
		it('422 - Admin should NOT POST a user with email that already exists', () => {
			const user = {
				firstName: faker.random.words(),
				lastName: faker.random.words(),
				email,
				password: 'qwertyuiop',
				role: 'admin',
				phone: faker.phone.phoneNumber()
			};
			return frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[0].token}`
						}
					}
				})
				.post(`${fullUrl}/user`, { body: qs.stringify(user) })
				.expect('status', 422);
		});
		it('422 - Admin should NOT POST a user with not known role', () => {
			const user = {
				firstName: faker.random.words(),
				lastName: faker.random.words(),
				email,
				password: 'qwertyuiop',
				role: faker.random.words(),
				phone: faker.phone.phoneNumber()
			};
			return frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[0].token}`
						}
					}
				})
				.post(`${fullUrl}/user`, { body: qs.stringify(user) })
				.expect('status', 422);
		});
	});

	describe('GET - User by Id', () => {
		it('200 - Admin should GET a admin by the given id', () =>
			frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[0].token}`
						}
					}
				})
				.get(`${fullUrl}/user/${tokens[0].user._id}`)
				.expect('status', 200)
				.expect('jsonTypes', '', asserts.user));
		it('200 - Admin should GET a user by the given id', () =>
			frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[0].token}`
						}
					}
				})
				.get(`${fullUrl}/user/${tokens[1].user._id}`)
				.expect('status', 200)
				.expect('jsonTypes', '', asserts.user));
	});

	describe('PATCH - User by Id', () => {
		it('200 - Admin should update the user by the given id', () => {
			const id = createdID.slice(-1).pop();
			const user = {
				firstName: 'firstName',
				lastName: 'lastName',
				// email: 'emailthatalreadyexists@email.com',
				role: 'admin',
				phone: faker.phone.phoneNumber()
			};
			return frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[0].token}`
						}
					}
				})
				.patch(`${fullUrl}/user/${id}`, { body: qs.stringify(user) })
				.expect('status', 200)
				.expect('jsonTypes', '', asserts.user)
				.expect('jsonTypes', '', [
					{ firstName: 'firstName', lastName: 'lastName' }
				]);
		});
		/* TODO it('200 - Admin should NOT UPDATE a user with email that already exists in database', () => {
			const id = createdID.slice(-1).pop();
			const user = {
				firstName: 'JS123456',
				lastName: 'JS123456',
				email: 'user@user.com',
				role: 'admin',
				phone: faker.phone.phoneNumber()
			};
			return frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[0].token}`
						}
					}
				})
				.patch(`${fullUrl}/user/${id}`, { body: qs.stringify(user) })
				.expect('status', 422);
		}); */
		it('404 - User should NOT UPDATE another user if not an admin', () => {
			const user = {
				firstName: 'JS123456',
				lastName: 'JS123456',
				email: 'user@user.com',
				role: 'admin',
				phone: faker.phone.phoneNumber()
			};
			return frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[1].token}`
						}
					}
				})
				.patch(`${fullUrl}/user/${tokens[0].user._id}`, {
					body: qs.stringify(user)
				})
				.expect('status', 404);
		});
	});

	describe('DELETE - Admin should delete a user by the given id', () => {
		it('200 - DELETE USER', () => {
			const id = createdID.slice(-1).pop();
			return frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[0].token}`
						}
					}
				})
				.del(`${fullUrl}/user/${id}`)
				.expect('status', 200);
		});
	});
});
