/* eslint handle-callback-err: "off"*/

process.env.NODE_ENV = 'test';

const frisby = require('frisby');
const qs = require('qs');
const fs = require('fs');
// const uuid = require('uuid');

const { config } = require('../base/config/test-config');
const fullUrl = `${config.url}:${config.port}`;

let tokens;

frisby.globalSetup({
	request: {
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}
});

describe('********************** TICKET TEST **********************', () => {
	it('Read tokens fron user_tokens.json file', done => {
		tokens = JSON.parse(fs.readFileSync('../json_files/user_tokens.json'));
		done();
	});

	describe('POST - ticket creation & access', () => {
		it('200 - User2 creates a ticket', () => {
			const ticket = {
				title: 'Ticket Test',
				description: 'Je suis une jolie description',
				status: 'open',
				subject: 'technical',
				priority: 'medium'
			};
			return frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[2].token}`
						}
					}
				})
				.post(`${fullUrl}/ticket`, { body: qs.stringify(ticket) })
				.expect('status', 201);
		});
		it('200 - User2 creates a ticket', () => {
			const ticket = {
				title: 'Ticket Test',
				description: 'Je suis une jolie description',
				status: 'open',
				subject: 'technical',
				priority: 'medium'
			};
			return frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[2].token}`
						}
					}
				})
				.post(`${fullUrl}/ticket`, { body: qs.stringify(ticket) })
				.expect('status', 201);
		});
		it('200 - Admin should GET a admin by the given id', () =>
			frisby
				.setup({
					request: {
						headers: {
							Authorization: `Bearer ${tokens[0].token}`
						}
					}
				})
				.get(`${fullUrl}/ticket`)
				.expect('status', 200)
				.then(resp => console.log(resp)));
	});
});
