const { findTicket } = require('../../ticket/ticket-service');
const { isIDGood } = require('../config/mongo-utils');
const { getValueByAddress } = require('../utils/validator-utils');
const { isAdmin } = require('../validators/user-validator');

async function checkTicketOwner(req, res, next) {
	const _id = isIDGood(req.params.id);
	const isUserAdmin = await isAdmin(isIDGood(req.user._id));
	const item = await findTicket({ _id }, '_id owner');
	if (
		!(
			!isUserAdmin &&
			getValueByAddress(item, 'owner') &&
			isIDGood(req.user._id) === isIDGood(item.owner)
		)
	) {
		res.status(404).json('ITEM_NOT_FOUND');
	}
	next();
}

module.exports = {
	checkTicketOwner
};
