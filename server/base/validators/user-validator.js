const { validateResult, buildErrObject } = require('../utils');
const { check } = require('express-validator');
const {
	saveLoginAttemptsToDB,
	blockUser,
	findUserById
} = require('../../skeleton/user/user-service');

/**
 * Validates forgot password request
 */
const validateForgotPassword = [
	check('email')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY')
		.isEmail()
		.withMessage('EMAIL_IS_NOT_VALID'),
	(req, res, next) => {
		validateResult(req, res, next);
	}
];

/**
 * Validates login request
 */
const validateLogin = [
	check('email')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY')
		.isEmail()
		.withMessage('EMAIL_IS_NOT_VALID'),
	check('password')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY')
		.isLength({ min: 5 })
		.withMessage('PASSWORD_TOO_SHORT_MIN_5'),
	(req, res, next) => {
		validateResult(req, res, next);
	}
];

/**
 * Validates register request
 */
const validateRegister = [
	check('firstName')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY'),
	check('lastName')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY'),
	check('email')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY')
		.isEmail()
		.withMessage('EMAIL_IS_NOT_VALID'),
	check('password')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY')
		.isLength({
			min: 5
		})
		.withMessage('PASSWORD_TOO_SHORT_MIN_5'),
	(req, res, next) => {
		validateResult(req, res, next);
	}
];

/**
 * Validates reset password request
 */
const validateResetPassword = [
	check('id')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY'),
	check('password')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY')
		.isLength({
			min: 5
		})
		.withMessage('PASSWORD_TOO_SHORT_MIN_5'),
	(req, res, next) => {
		validateResult(req, res, next);
	}
];

/**
 * Validates verify request
 */
const validateVerify = [
	check('id')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY'),
	(req, res, next) => {
		validateResult(req, res, next);
	}
];

/**
 * Validates create new item request
 */
const validateCreateUser = [
	check('firstName')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY'),
	check('lastName')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY'),
	check('email')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY')
		.isEmail()
		.withMessage('EMAIL_IS_NOT_VALID'),
	check('password')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY')
		.isLength({
			min: 5
		})
		.withMessage('PASSWORD_TOO_SHORT_MIN_5'),
	check('role')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY')
		.isIn(['user', 'admin'])
		.withMessage('USER_NOT_IN_KNOWN_ROLE'),
	check('phone')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY')
		.trim(),
	(req, res, next) => {
		validateResult(req, res, next);
	}
];

/**
 * Validates delete item request
 */
const validateDeleteUser = [
	check('id')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY'),
	(req, res, next) => {
		validateResult(req, res, next);
	}
];

/**
 * Validates get item request
 */
const validateGetUser = [
	check('id')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY'),
	(req, res, next) => {
		validateResult(req, res, next);
	}
];

/**
 * Validates update item request
 */
const validateUpdateUser = [
	check('firstName')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY'),
	check('lastName')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY'),
	/* check('email')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY'),
	check('role')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY'),*/
	check('phone')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY')
		.trim(),
	check('id')
		.exists()
		.withMessage('MISSING')
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY'),
	(req, res, next) => {
		validateResult(req, res, next);
	}
];

/**
 * Validates change password request
 */
const validateChangePassword = [
	check('oldPassword')
		.optional()
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY')
		.isLength({ min: 5 })
		.withMessage('PASSWORD_TOO_SHORT_MIN_5'),
	check('newPassword')
		.optional()
		.not()
		.isEmpty()
		.withMessage('IS_EMPTY')
		.isLength({ min: 5 })
		.withMessage('PASSWORD_TOO_SHORT_MIN_5'),
	(req, res, next) => {
		validateResult(req, res, next);
	}
];

/**
 * Adds one attempt to loginAttempts, then compares loginAttempts with the constant LOGIN_ATTEMPTS, if is less returns wrong password, else returns blockUser function
 * @param {Object} user - user object
 */
async function passwordsDoNotMatch(user = {}) {
	return new Promise(async (resolve, reject) => {
		try {
			user.loginAttempts += 1;
			await saveLoginAttemptsToDB(user);
			if (user.loginAttempts <= process.env.LOGIN_ATTEMPTS) {
				return reject(buildErrObject(409, 'WRONG_PASSWORD'));
			}

			resolve(await blockUser(user));
		} catch (error) {
			throw error;
		}
	});
}

async function isAdmin(userId = '') {
	const user = await findUserById(userId, 'role');
	return user.role === 'admin';
}

module.exports = {
	validateForgotPassword,
	validateLogin,
	validateRegister,
	validateResetPassword,
	validateVerify,
	validateCreateUser,
	validateDeleteUser,
	validateGetUser,
	validateUpdateUser,
	validateChangePassword,
	passwordsDoNotMatch,
	isAdmin
};
