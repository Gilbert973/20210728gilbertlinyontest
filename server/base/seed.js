require('dotenv-safe').config();
const path = require('path');
const mongoose = require('mongoose');
const initMongo = require('../base/config/mongo');

const { getFilesByType } = require('./utils/index');

function toTitleCase(str) {
	return str.replace(
		/\w\S*/g,
		txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
	);
}

async function main() {
	try {
		initMongo();
		const seeds = getFilesByType('./', 'seed');
		for (const seed of seeds) {
			const model = mongoose.models[toTitleCase(seed.module)];
			if (!model) {
				console.log(`Cannot find Model ${toTitleCase(seed.module)}`);
				continue;
			}
			const fileContents = require(path.join(`../${seed.fullPath}`));
			await model.insertMany(fileContents);
		}
		// await seeder.import(collections);
		console.log('Seed complete!');
		process.exit(0);
	} catch (err) {
		console.log(err);
		process.exit(0);
	}
}

main();
