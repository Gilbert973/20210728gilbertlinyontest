const fs = require('fs');
const path = require('path');

const files = [];

function getFilesByType(dir, fileType = 'model', firstCall = true) {
	if (firstCall) files.length = 0;

	fs.readdirSync(dir).forEach(file => {
		const fullPath = path.join(dir, file);
		if (fs.lstatSync(fullPath).isDirectory()) {
			getFilesByType(fullPath, fileType, false);
		} else if (file.endsWith(`-${fileType}.js`)) {
			const splittedName = fullPath.split('/');
			files.push({
				fullPath,
				module: splittedName[splittedName.length - 2]
			});
		}
	});
	return files;
}
module.exports = { getFilesByType };
