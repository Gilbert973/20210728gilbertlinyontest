const fs = require('fs');
const utilsPath = `${__dirname}/`;
const { removeExtensionFromFile } = require('./removeExtensionFromFile');

const modules = {};

fs.readdirSync(utilsPath).forEach(file => {
	const utilFile = removeExtensionFromFile(file);
	// Prevents loading of this file
	if (utilFile !== 'index' && utilFile !== 'handleError.test') {
		Object.assign(modules, require(`./${utilFile}`));
	}
});

module.exports = modules;
