module.exports = {
	buildErrObject,
	buildSuccObject,
	buildErrMessage
};

/**
 * Builds error object
 * @param {number} code - error code
 * @param {string} message - error text
 */
function buildErrObject(code = '', message = '') {
	return { code, message };
}

/**
 * Builds success object
 * @param {string} message - success text
 */
function buildSuccObject(message = '') {
	return { msg: message };
}

/**
 * Builds custom error message
 * @param {number} code - error code
 * @param {string} message - error text
 */
function buildErrMessage(name, code = '', data = {}) {
	return { name, code, data };
}
