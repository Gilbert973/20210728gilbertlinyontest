const { isset } = require('./validator-utils');

module.exports = {
	round,
	random,
	cln,
	pad,
	mergeMixins,
	getObjFromCLIparams,
	minMax,
	generateToken,
	moyenne: average,
	average,
	sumArray,
	urlPathJoin,
	miniTemplater,
	isBetween,
	simpleObjectMaskOrSelect,
	parseEnv,
	parseBool,
	objectToUrlParams,
	// ALIASES
	int: parseInt,
	clamp: minMax
};

/** Round with custom number of decimals (default:0) */
function round(number, decimals = 0) {
	return Math.round(number * Math.pow(10, decimals)) / Math.pow(10, decimals);
}

/** Is number between two numbers (including those numbers) */
function isBetween(number, min, max) {
	return number <= max && number >= min;
}

/** Random number between two values */
function random(nb1, nb2, nbOfDecimals) {
	return round(Math.random() * (nb2 - nb1) + nb1, nbOfDecimals);
}

/** Sum all values of an array, all values MUST be numbers */
function sumArray(array) {
	return array
		.filter(item => typeof item === 'number')
		.reduce((sum, val) => (isset(val) ? val + sum : sum), 0);
}

/** Moyenne / average between array of values
 * @param {Number} round number of decimals to keep. Default:2
 */
function average(array, nbOfDecimals = 2) {
	return round(sumArray(array) / array.length, nbOfDecimals);
}

/** Clean output for outside world. All undefined / null / NaN / Infinity values are changed to '-' */
function cln(val, replacerInCaseItIsUndefinNaN = '-') {
	return [
		'undefined',
		undefined,
		'indéfini',
		'NaN',
		NaN,
		Infinity,
		null
	].includes(val)
		? replacerInCaseItIsUndefinNaN
		: val;
}

/** length default 2, shortcut for 1 to 01 */
function pad(numberOrStr, length = 2) {
	return `${numberOrStr}`.padStart(length, 0);
}

/** return the number or the closest number of the range
 * * nb min max  => returns
 * * 7  5   10   => 7 // in the range
 * * 2  5   10   => 5 // below the min value
 * * 99 5   10   => 10// above the max value
 */
function minMax(nb, min, max) {
	return Math.max(min, Math.min(nb, max));
}

function mergeMixins(thing, ...mixins) {
	mixins.forEach(mixin => {
		for (const method in mixin) {
			thing[method] = mixin[method];
		}
	});
}

/**
 * @param {array} argsWithNoValue array of params with no value (will be param: true in object). Eg: --exitOnFail OR --dropDB
 * @return {obj} {myParam: value, strings: [array of orphan strings (eg: blah --param value => blah will be orphan)]}
 */
function getObjFromCLIparams(argsWithNoValue) {
	const processArgs = JSON.parse(JSON.stringify(process.argv));
	if (processArgs[1] && processArgs[1].includes('/')) {
		processArgs.splice(0, 2);
	}
	// first 2 are path
	else {
		processArgs.splice(0, 1);
	} // edge case...

	// get all args as arg => value
	const args = {};
	processArgs.forEach((arg, i) => {
		if (/^--/.test(arg)) {
			const argName = arg.replace('--', '');
			if (argsWithNoValue.includes(argName)) {
				args[argName] = true;
			} else {
				args[argName] = processArgs[i + 1];
				processArgs[i + 1] = null;
			}
			processArgs[i] = null;
		}
	});

	args.strings = processArgs.filter(test => test); //

	return args;
}

const generatedTokens = []; // timestamp, counter
/** minLength 8 if unique
 * @param {Number} length default: 20
 * @param {Boolean} unique default: true. Generate a real unique token base on the date. min length will be min 8 in this case
 * @param {string} mode one of ['alphanumeric', 'hexadecimal', 'numeric']
 * NOTE: to generate a mongoDB Random Id, use the params: 24, true, 'hexadecimal'
 */
function generateToken(length = 20, unique, mode = 'alphanumeric') {
	let uniqueDefault = false;
	if (!isset(unique)) {
		unique = true;
		uniqueDefault = true;
	}
	const charConvNumeric =
		mode === 'alphanumeric' ? 36 : mode === 'numeric' ? 10 : 16;
	if (!uniqueDefault && unique && length < 8) {
		length = 8;
	}
	let token = unique ? new Date().getTime().toString(charConvNumeric) : '';
	while (token.length < length) {
		token += Math.random().toString(charConvNumeric).substr(2, 1);
	} // char alphaNumeric aléatoire
	while (generatedTokens.includes(token)) {
		token = token.replace(
			/.$/,
			Math.random().toString(charConvNumeric).substr(2, 1)
		);
	}
	return token;
}

/** Useful to join differents bits of url with normalizing slashes
 * * urlPathJoin('https://', 'www.kikou.lol/', '/user', '//2//') => https://www.kikou.lol/user/2/
 * * urlPathJoin('http:/', 'kikou.lol') => https://www.kikou.lol
 */
function urlPathJoin(...bits) {
	return bits
		.join('/')
		.replace(/\/+/g, '/')
		.replace(/(https?:)\/\/?/, '$1//');
}

/**
 * Replace variables in a string like: `Hello {{userName}}!`
 * @param {String} content
 * @param {Object} varz object with key => value === toReplace => replacer
 * @param {Object} options
 * * valueWhenNotSet => replacer for undefined values. Default: ''
 * * regexp          => must be 'g' and first capturing group matching the value to replace. Default: /{{\s*([^}]*)\s*}}/g
 */
function miniTemplater(content, varz, options) {
	options = {
		valueWhenNotSet: '',
		regexp: /{{\s*([^}]*)\s*}}/g,
		...options
	};
	return content.replace(options.regexp, (m, $1) =>
		isset(varz[$1]) ? varz[$1] : options.valueWhenNotSet
	);
}

/**
 *
 * @param {Object} object main object
 * @param {String[]} maskedOrSelectedFields array of fields
 * @param {Boolean} isMask default: true; determine the behavior of the function. If is mask, selected fields will not appear in the resulting object. If it's a select, only selected fields will appear.
 * @param {Boolean} deleteKeysInsteadOfReturningAnewObject default:false; modify the existing object instead of creating a new instance
 */
function simpleObjectMaskOrSelect(
	object,
	maskedOrSelectedFields,
	isMask = true,
	deleteKeysInsteadOfReturningAnewObject = false
) {
	const allKeys = Object.keys(object);
	const keysToMask = allKeys.filter(keyName => {
		if (isMask) {
			return maskedOrSelectedFields.includes(keyName);
		} else {
			return !maskedOrSelectedFields.includes(keyName);
		}
	});
	if (deleteKeysInsteadOfReturningAnewObject) {
		keysToMask.forEach(keyNameToDelete => delete object[keyNameToDelete]);
		return object;
	} else {
		return allKeys.reduce((newObject, key) => {
			if (!keysToMask.includes(key)) {
				newObject[key] = object[key];
			}
			return newObject;
		}, {});
	}
}

/** Parse one dimension object undefined, true, false, null represented as string will be converted to primitives */
function parseEnv(env) {
	const newEnv = {};
	for (const k in env) {
		const val = env[k];
		if (val === 'undefined') {
			newEnv[k] = undefined;
		} else if (val === 'true') {
			newEnv[k] = true;
		} else if (val === 'false') {
			newEnv[k] = false;
		} else if (val === 'null') {
			newEnv[k] = null;
		} else {
			newEnv[k] = env[k];
		}
	}
	return newEnv;
}

/**
 * @param {Any} mayBeAstring
 * @return {Boolean} !!value
 */
function parseBool(mayBeAstring) {
	if (typeof mayBeAstring === 'boolean') {
		return mayBeAstring;
	} else {
		return !!mayBeAstring;
	}
}

/** Parse params object to url
 * @param {Object} params  {myfield1: value1, myfield2: value2}
 * @returns {String} - ?myfield1=value1&myfield2=value2
 */
function objectToUrlParams(params = {}) {
	let uriParams = '';
	for (const [key, value] of Object.entries(params)) {
		uriParams += `&${key}=${value}`;
	}
	return uriParams;
}
