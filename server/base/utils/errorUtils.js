const { buildErrObject } = require('./objectUtils');
const { isset, isEmptyOrNotSet, isEmpty } = require('./validator-utils');

module.exports = {
	handleError,
	itemNotFound
};

/**
 * Handles error by printing to console in development env and builds and sends an error response
 * @param {Object} res - response object
 * @param {Object} err - error object
 */
function handleError(res = {}, err = {}) {
	// Prints error in console
	if (process.env.NODE_ENV === 'development') {
		console.log(err);
	}
	// Sends error to user
	if (!isset(err.code)) err.code = 500;
	return res.status(err.code).json({
		errors: {
			msg: err.message
		}
	});
}

/**
 * Item not found
 * @param {Object} err - error object
 * @param {Object} item - item result object
 * @param {string} message - message
 */
function itemNotFound(err = {}, item = {}, message = 'NOT_FOUND') {
	return new Promise((resolve, reject) => {
		if (!isEmptyOrNotSet(err)) {
			return reject(buildErrObject(422, err.message));
		}
		if (isEmpty(item)) {
			return reject(buildErrObject(404, message));
		}
		resolve();
	});
}
