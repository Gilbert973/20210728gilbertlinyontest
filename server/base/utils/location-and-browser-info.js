const requestIp = require('request-ip');

module.exports = {
	getBrowserInfo,
	getCountry,
	getIP
};

/**
 * Gets browser info from user
 * @param {*} req - request object
 */
function getBrowserInfo({ headers }) {
	return headers['user-agent'];
}

/**
 * Gets country from user using CloudFlare header 'cf-ipcountry'
 * @param {*} req - request object
 */
function getCountry({ headers }) {
	return headers['cf-ipcountry'] ? headers['cf-ipcountry'] : 'XX';
}

/**
 * Gets IP from user
 * @param {*} req - request object
 */
function getIP(req) {
	return requestIp.getClientIp(req);
}
