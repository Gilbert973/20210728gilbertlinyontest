module.exports = {
	isType,
	isDateObject,
	isset,
	issetOr,
	isEmptyOrNotSet,
	isEmpty,
	checkAllObjectValuesAreEmpty,
	isUndefined,
	isDefined,
	// ALIASES
	orIsset: issetOr,
	checkModelProperties,
	getValueByAddress
};

function isset(...elms) {
	return elms.every(elm => typeof elm !== 'undefined' && elm !== null);
}

function issetOr(...elms) {
	return elms.some(elm => typeof elm !== 'undefined' && elm !== null);
}

function isEmptyOrNotSet(...elms) {
	return elms.some(elm => !isset(elm) || isEmpty(elm));
}

function isEmpty(objOrArr) {
	if (Array.isArray(objOrArr) || typeof objOrArr === 'string') {
		return objOrArr.length === 0;
	} else if (typeof objOrArr === 'object') {
		return (
			Object.keys(objOrArr).length === 0 &&
			objOrArr.constructor === Object
		);
	}
}
/**
 * isType return true if obj is of class klass or a superclass of klass (such as Object).
 * If type is a Function, will match if obj instanceof type (includes subclasses).
 * If type is a String, will match if typeof(obj) === type or obj.constructor.name === type,
 * so if tom is a Horse, which is a subclass of Animal,
 * then isClass(tom, "Animal") is false but isClass(tom, Animal) is true.
 * @Param {obj} Object to test
 * @Param {type} Function or String with name or constructor of object.
 * @Return {boolean} true if obj is of given type, using typeof, instanceof, or obj.constructor.
 */
function isType(obj, type) {
	if (obj === null) {
		return type === 'null' || type === 'object' || type === null;
	}
	if (typeof obj === 'undefined') {
		return type === 'undefined' || typeof type === 'undefined';
	}
	return (
		typeof obj === type ||
		obj.constructor === type ||
		(typeof type === 'function'
			? obj instanceof type
			: getType(obj) === type)
	);
}

// get constructor or typeof. Note that getType(null) == "object".
function getType(obj) {
	return (obj !== null && getName(obj.constructor)) || typeof obj;
}

function getName(cons) {
	return cons.name || getFunc(cons);
}

// Hack for IE 7 to find .constructor.name
function getFunc(func) {
	const m = func.toString().match(/function ([^(]*)/);
	return (m && m.length > 1 && m[1]) || func; /* .toString() */
}

function isDateObject(variable) {
	return variable instanceof Date;
}

/** Check all values are set */
function checkAllObjectValuesAreEmpty(o) {
	return Object.values(o).every(value => !isset(value));
}

/** This use the good practice typeof elm !== 'undefined' */
function isUndefined(val) {
	return typeof val !== 'undefined';
}
function isDefined(val) {
	return !isUndefined(val);
}

function getValueByAddress(obj, address = '') {
	if (isEmptyOrNotSet(address)) return obj;
	address = address.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
	address = address.replace(/^\./, ''); // strip a leading dot
	const a = address.split('.');
	for (let i = 0, n = a.length; i < n; ++i) {
		const k = a[i];
		if (k in obj) {
			obj = obj[k];
		} else {
			return;
		}
	}
	return obj;
}

function checkModelProperties(obj, model, excludeFields = []) {
	for (const path in model.schema.paths) {
		if (
			isEmptyOrNotSet(getValueByAddress(obj, path)) &&
			!excludeFields.includes(path)
		)
			return false;
	}
	return true;
}
