const { isType } = require('../utils/validator-utils');

module.exports = {
	isIDGood
};

/**
 * Checks if given ID is good for MongoDB
 * @param {Object | String} obj - id to check
 */
function isIDGood(obj = {}) {
	if (!obj) return; // null case
	if (obj._id) return obj._id.toString();
	else if (isType(obj, 'objectId')) return obj.toString();
	else if (isType(obj, 'String')) return obj;
}
