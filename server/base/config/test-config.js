const { Joi } = require('frisby');

const loginDetails = {
	admin: {
		id: '5aa1c2c35ef7a4e97b5e995a',
		email: 'admin@admin.com',
		password: '12345'
	},
	user: {
		id: '5aa1c2c35ef7a4e97b5e995b',
		email: 'user@user.com',
		password: '12345'
	}
};

const config = {
	url: 'http://localhost',
	port: '3111'
};

const user = Joi.object({
	_id: Joi.string().required(),
	email: Joi.string().required(),
	role: Joi.string().required(),
	verified: Joi.boolean().required(),
	verification: Joi.string().required()
});

const auth = Joi.object({
	token: Joi.string().required(),
	user: Joi.object().required()
});

const verify = Joi.object({
	email: Joi.string().required(),
	verified: Joi.boolean().equal(true)
});

const forgotPwd = Joi.object({
	msg: Joi.string().required(),
	email: Joi.string().required(),
	verification: Joi.string().required()
});

const refresh = Joi.object({
	token: Joi.string().required()
});

const docs = Joi.object({ docs: Joi.array().required() });

const asserts = { auth, user, verify, forgotPwd, refresh, docs };

module.exports = {
	loginDetails,
	config,
	asserts
};
