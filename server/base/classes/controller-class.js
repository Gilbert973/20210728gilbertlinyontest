const User = require('../../skeleton/user/user-model');
const { itemNotFound, buildErrObject } = require('../../base/utils');
const { handleError } = require('../utils/errorUtils');
const { matchedData } = require('express-validator');
const { isIDGood } = require('../config/mongo-utils');
const { updateItem } = require('../middleware/db');

module.exports = class ControllerClass {
	checkPermissions({ id = '', roles = [] }, next) {
		return new Promise((resolve, reject) => {
			User.findById(id, async (err, result) => {
				try {
					await itemNotFound(err, result, 'USER_NOT_FOUND');
					if (roles.indexOf(result.role) > -1) {
						return resolve(next());
					}
					reject(buildErrObject(401, 'UNAUTHORIZED'));
				} catch (error) {
					reject(error);
				}
			});
		});
	}

	roleAuthorization = roles => async (req, res, next) => {
		try {
			const data = { id: req.user._id, roles };
			await this.checkPermissions(data, next);
		} catch (error) {
			handleError(res, error);
		}
	};

	/**
	 * Checks if user has access to the resource
	 */
	async checkUserId(req, res, next) {
		return new Promise((resolve, reject) => {
			try {
				if (
					req.user.role === 'user' &&
					isIDGood(req.params.id) !== isIDGood(req.user._id)
				)
					return res
						.status(404)
						.json(buildErrObject(404, 'NOT_FOUND'));
				return resolve(next());
			} catch (error) {
				reject(res, error);
			}
		});
	}

	/**
	 * Checks if blockExpires from user is greater than now
	 * @param {Object} user - user object
	 */
	userIsBlocked(user = {}) {
		return new Promise((resolve, reject) => {
			if (user.blockExpires > new Date()) {
				return reject(buildErrObject(409, 'BLOCKED_USER'));
			}
			resolve(true);
		});
	}

	/**
	 * Display false
	 * @param {Object} req - request object
	 * @param {Object} res - response object
	 */
	display = model => async (req, res) => {
		try {
			req = matchedData(req);
			const id = isIDGood(req.id);
			return res.status(200).json(await updateItem(id, model, req));
		} catch (error) {
			handleError(res, error);
		}
	};
};
