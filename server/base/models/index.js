const { getFilesByType } = require('../utils/files-by-type');

const models = getFilesByType('./', 'model');

module.exports = () => {
	models.forEach(model => {
		require(`../../${model.fullPath}`);
	});
};
