const { buildErrObject } = require('../../utils/objectUtils');
const crypto = require('crypto');

module.exports = {
	checkPassword,
	decrypt,
	encrypt
};

const secret = process.env.JWT_SECRET;
const algorithm = 'aes-256-cbc';
// Key length is dependent on the algorithm. In this case for aes256, it is
// 32 bytes (256 bits).
const key = crypto.scryptSync(secret, 'salt', 32);
const iv = Buffer.alloc(16, 0); // Initialization crypto vector

/**
 * Checks is password matches
 * @param {string} password - password
 * @param {Object} user - user object
 * @returns {boolean}
 */
function checkPassword(password = '', user = {}) {
	return new Promise((resolve, reject) => {
		user.comparePassword(password, (err, isMatch) => {
			if (err) {
				return reject(buildErrObject(422, err.message));
			}
			if (!isMatch) {
				resolve(false);
			}
			resolve(true);
		});
	});
}

/**
 * Decrypts text
 * @param {string} text - text to decrypt
 */
function decrypt(text = '') {
	const decipher = crypto.createDecipheriv(algorithm, key, iv);

	try {
		let decrypted = decipher.update(text, 'hex', 'utf8');
		decrypted += decipher.final('utf8');
		return decrypted;
	} catch (err) {
		return err;
	}
}

/**
 * Encrypts text
 * @param {string} text - text to encrypt
 */
function encrypt(text = '') {
	const cipher = crypto.createCipheriv(algorithm, key, iv);

	let encrypted = cipher.update(text, 'utf8', 'hex');
	encrypted += cipher.final('hex');

	return encrypted;
}
