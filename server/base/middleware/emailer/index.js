const User = require('../../../skeleton/user/user-model');

const i18n = require('i18n');
const nodemailer = require('nodemailer');
const mg = require('nodemailer-mailgun-transport');

const { buildErrObject } = require('../../../base/utils/objectUtils');

module.exports = {
	emailExists,
	emailExistsExcludingMyself,
	prepareToSendEmail,
	sendEmail,
	sendRegistrationEmailMessage,
	sendResetPasswordEmailMessage
};

/**
 * Checks User model if user with an specific email exists
 * @param {string} email - user email
 */
function emailExists(email = '') {
	return new Promise((resolve, reject) => {
		User.findOne({ email }, (err, item) => {
			if (err) return reject(buildErrObject(422, err.message));
			if (item)
				return reject(buildErrObject(422, 'EMAIL_ALREADY_EXISTS'));
			resolve(false);
		});
	});
}

/**
 * Checks User model if user with an specific email exists but excluding user id
 * @param {string} id - user id
 * @param {string} email - user email
 */
function emailExistsExcludingMyself(id = '', email = '') {
	return new Promise((resolve, reject) => {
		User.findOne({ email, _id: { $ne: id } }, async (err, item) => {
			if (err) return reject(buildErrObject(422, err.message));
			if (item)
				return reject(buildErrObject(422, 'EMAIL_ALREADY_EXISTS'));
			resolve(false);
		});
	});
}

/**
 * Prepares to send email
 * @param {string} user - user object
 * @param {string} subject - subject
 * @param {string} htmlMessage - html message
 */
function prepareToSendEmail(user = {}, subject = '', htmlMessage = '') {
	const data = {
		user,
		subject,
		htmlMessage
	};
	if (process.env.NODE_ENV === 'production') {
		sendEmail(data, messageSent =>
			messageSent
				? console.log(`Email SENT to: ${user.email}`)
				: console.log(`Email FAILED to: ${user.email}`)
		);
	} else if (process.env.NODE_ENV === 'development') console.log(data);
}

/**
 * Sends email
 * @param {Object} data - data
 * @param {boolean} callback - callback
 */
async function sendEmail(data = {}, callback) {
	const auth = {
		auth: {
			// eslint-disable-next-line camelcase
			api_key: process.env.EMAIL_SMTP_API_MAILGUN,
			domain: process.env.EMAIL_SMTP_DOMAIN_MAILGUN
		}
		// host: 'api.eu.mailgun.net' // THIS IS NEEDED WHEN USING EUROPEAN SERVERS
	};
	const transporter = nodemailer.createTransport(mg(auth));
	const mailOptions = {
		from: `${process.env.EMAIL_FROM_NAME} <${process.env.EMAIL_FROM_ADDRESS}>`,
		to: `${data.user.name} <${data.user.email}>`,
		subject: data.subject,
		html: data.htmlMessage
	};
	transporter.sendMail(mailOptions, err => {
		if (err) return callback(false);
		return callback(true);
	});
}

/**
 * Sends registration email
 * @param {string} locale - locale
 * @param {Object} user - user object
 */
function sendRegistrationEmailMessage(locale = '', user = {}) {
	i18n.setLocale(locale);
	const subject = i18n.__('registration.SUBJECT');
	const htmlMessage = i18n.__(
		'registration.MESSAGE',
		`${user.firstName} ${user.lastName}`,
		process.env.FRONTEND_URL,
		user.verification
	);
	prepareToSendEmail(user, subject, htmlMessage);
}

/**
 * Sends reset password email
 * @param {string} locale - locale
 * @param {Object} user - user object
 */
function sendResetPasswordEmailMessage(locale = '', forgot = {}) {
	i18n.setLocale(locale);
	const subject = i18n.__('forgotPassword.SUBJECT');
	const htmlMessage = i18n.__(
		'forgotPassword.MESSAGE',
		forgot.user.email,
		process.env.FRONTEND_URL,
		forgot.verification
	);
	prepareToSendEmail(forgot, subject, htmlMessage);
}
