const {
	buildErrObject,
	buildSuccObject,
	itemNotFound
} = require('../../utils');

module.exports = {
	buildSort,
	checkQueryString,
	cleanPaginationID,
	createItem,
	deleteItem,
	getItem,
	getItems,
	listInitOptions,
	updateItem,
	deepCleanPagination
};

/**
 * Builds sorting
 * @param {string} sort - field to sort from
 * @param {number} order - order for query (1,-1)
 */
function buildSort(sort = '', order = 1) {
	const sortBy = {};
	sortBy[sort] = order;
	return sortBy;
}

/**
 * Checks the query string for filtering records
 * query.filter should be the text to search (string)
 * query.fields should be the fields to search into (array)
 * @param {Object} query - query object
 */
function checkQueryString(query = {}) {
	return new Promise((resolve, reject) => {
		try {
			if (
				typeof query.filter !== 'undefined' &&
				typeof query.fields !== 'undefined'
			) {
				const data = {
					$or: []
				};
				const array = [];
				// Takes fields param and builds an array by splitting with ','
				const arrayFields = query.fields.split(',');
				// Adds SQL Like %word% with regex
				arrayFields.map(item => {
					array.push({
						[item]: {
							$regex: new RegExp(query.filter, 'i')
						}
					});
				});
				// Puts array result in data
				data.$or = array;
				resolve(data);
			} else {
				resolve({});
			}
		} catch (err) {
			console.log(err.message);
			reject(buildErrObject(422, 'ERROR_WITH_FILTER'));
		}
	});
}

/**
 * Hack for mongoose-paginate, removes 'id' from results
 * @param {Object} result - result object
 */
function cleanPaginationID(result = {}) {
	result.docs.map(element => delete element.id);
	return result;
}

/**
 * Hack for mongoose-paginate, removes 'id' from results
 * @param {Object} result - result object
 */
function deepCleanPagination(result = {}) {
	result = cleanPaginationID(result);
	delete result.totalDocs;
	delete result.limit;
	delete result.totalPages;
	delete result.pagingCounter;
	delete result.hasPrevPage;
	delete result.hasNextPage;
	delete result.prevPage;
	delete result.nextPage;
	delete result.page;
	return result;
}

/**
 * Creates a new item in database
 * @param {Object} req - request object
 */
function createItem(req = {}, model = {}) {
	return new Promise((resolve, reject) => {
		model.create(req, (err, item) => {
			if (err) {
				reject(buildErrObject(422, err.message));
			}
			resolve(item);
		});
	});
}

/**
 * Deletes an item from database by id
 * @param {string} id - id of item
 */
function deleteItem(id = '', model = {}) {
	return new Promise((resolve, reject) => {
		model.findByIdAndRemove(id, async (err, item) => {
			try {
				await itemNotFound(err, item, 'NOT_FOUND');
				resolve(buildSuccObject('DELETED'));
			} catch (error) {
				reject(error);
			}
		});
	});
}

/**
 * Gets item from database by id
 * @param {string} id - item id
 */
function getItem(id = '', model = {}) {
	return new Promise((resolve, reject) => {
		model.findById(id, async (err, item) => {
			try {
				await itemNotFound(err, item, 'NOT_FOUND');
				resolve(item);
			} catch (error) {
				reject(error);
			}
		});
	});
}

/**
 * Gets items from database
 * @param {Object} req - request object
 * @param {Object} query - query object
 */
async function getItems(req = {}, model = {}, query = {}, deepClean = false) {
	const options = await listInitOptions(req);
	return new Promise((resolve, reject) => {
		model.paginate(query, options, (err, items) => {
			if (err) {
				return reject(buildErrObject(422, err.message));
			}
			resolve(
				!deepClean
					? cleanPaginationID(items)
					: deepCleanPagination(items)
			);
		});
	});
}

/**
 * Builds initial options for query
 * @param {Object} query - query object
 */
function listInitOptions(req = {}) {
	return new Promise(async (resolve, reject) => {
		try {
			const order = req.query.order || -1;
			const sort = req.query.sort || 'createdAt';
			const sortBy = buildSort(sort, order);
			const page = parseInt(req.query.page, 10) || 1;
			const limit = parseInt(req.query.limit, 10) || 5;
			const options = {
				sort: sortBy,
				lean: true,
				page,
				limit
			};
			resolve(options);
		} catch (error) {
			console.log(error.message);
			reject(buildErrObject(422, 'ERROR_WITH_INIT_OPTIONS'));
		}
	});
}

/**
 * Updates an item in database by id
 * @param {string} id - item id
 * @param {Object} req - request object
 */
function updateItem(id = '', model = {}, req = {}) {
	return new Promise((resolve, reject) => {
		model.findByIdAndUpdate(
			id,
			req,
			{
				new: true,
				runValidators: true
			},
			async (err, item) => {
				try {
					await itemNotFound(err, item, 'NOT_FOUND');
					resolve(item);
				} catch (error) {
					reject(error);
				}
			}
		);
	});
}
