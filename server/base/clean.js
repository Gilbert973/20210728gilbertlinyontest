require('dotenv-safe').config();
const initMongo = require('./config/mongo');
const { getFilesByType } = require('./utils/index');
const serverPath = `./`;
const path = require('path');

initMongo();

function deleteModelFromDB(model) {
	return new Promise((resolve, reject) => {
		model = require(path.resolve(model));
		model.deleteMany({}, (err, row) => {
			if (err) {
				reject(err);
			} else {
				resolve(row);
			}
		});
	});
}

async function clean() {
	try {
		const models = getFilesByType(serverPath, 'model');
		const promiseArray = models.map(
			async model => await deleteModelFromDB(model.fullPath)
		);
		await Promise.all(promiseArray);
		console.log('Cleanup complete!');
		process.exit(0);
	} catch (err) {
		console.log(err);
		process.exit(0);
	}
}

clean();
