const express = require('express');
const router = express.Router();
const { getFilesByType } = require('../utils/index');
const { isset } = require('../utils/validator-utils');
const serverPath = `./`;

/*
 * Load routes statically and/or dynamically
 */
// router.use('/', require('../../user/user-route'));

const routes = [];
const routeFiles = getFilesByType(serverPath, 'route');
routeFiles.forEach(route => {
	if (route !== 'index' && route !== '.DS_Store') {
		const apiRoute = require(`../../${route.fullPath}`);
		['GET', 'POST', 'PATCH', 'DELETE'].forEach(method => {
			if (isset(apiRoute[method])) {
				for (const uri in apiRoute[method]) {
					const fullUri = `${apiRoute.baseUri}${uri}`;
					const middlewares = apiRoute[method][uri];
					routes.push([
						method.toLowerCase(),
						fullUri,
						...middlewares
					]);
				}
			}
		});
	}
});

// Routes registration
for (const [method, route, ...middleWares] of routes) {
	router[method](route, ...middleWares);
}

/*
 * Setup routes for index
 */
router.get('/', (req, res) => {
	res.render('index');
});

/*
 * Handle 404 error
 */
router.use('*', (req, res) => {
	res.status(404).json({
		errors: {
			msg: 'URL_NOT_FOUND'
		}
	});
});

module.exports = router;
