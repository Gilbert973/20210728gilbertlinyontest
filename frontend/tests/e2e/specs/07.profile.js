import faker from 'faker'

describe('Profile', () => {
  it('Visits the profile url', () => {
    cy.login('admin@admin.com')
    cy.visit('/profile')
    // url should be /profile
    cy.url().should('include', '/profile')
    cy.get('input[name=email]').should('have.value', 'admin@admin.com')
    // Logout
    cy.logout()
  })
  it('Checks input types', () => {
    cy.login('admin@admin.com')
    cy.visit('/profile')
    // Checks input type is email
    cy.get('input[name=email]')
      .invoke('attr', 'type')
      .should('contain', 'email')
    // Checks input email is disabled
    cy.get('input[name=email]')
      .invoke('attr', 'disabled')
      .should('contain', 'disabled')
    // Checks input type is tel
    cy.get('input[name=phone]').invoke('attr', 'type').should('contain', 'tel')
    // Logout
    cy.logout()
  })
  // eslint-disable-next-line max-statements
  it('Edits profile', () => {
    cy.login('admin@admin.com')
    cy.setLocaleToEN()
    cy.visit('/profile')

    const firstname = faker.name.firstName()
    const lastName = faker.name.lastName()
    const phone = faker.phone.phoneNumber()
    // firstname
    cy.get('input[name=firstname]').clear().type(firstname)
    // lastName
    cy.get('input[name=lastName]').clear().type(lastName)
    // phone
    cy.get('input[name=phone]').clear().type(phone)
    // Save button
    cy.get('button.btnSave').click()
    cy.get('div.success')
      .should('be.visible')
      .contains('Profile saved successfuly')
    // Confirm values
    cy.get('input[name=name]').should('have.value', name)
    cy.get('input[name=phone]').should('have.value', phone)
    // Logout
    cy.logout()
  })
  it('Checks input types for change password', () => {
    cy.login('admin@admin.com')
    cy.visit('/profile')
    cy.get('button.btnChangePassword').click()
    cy.get('div.v-dialog.v-dialog--active').should('be.visible')
    // Checks input type is password
    cy.get('input[name=oldPassword]')
      .invoke('attr', 'type')
      .should('contain', 'password')
    cy.get('input[name=newPassword]')
      .invoke('attr', 'type')
      .should('contain', 'password')
    cy.get('input[name=confirmPassword]')
      .invoke('attr', 'type')
      .should('contain', 'password')
  })
  it('Displays errors when current password is wrong', () => {
    cy.login('admin@admin.com')
    cy.setLocaleToEN()
    cy.visit('/profile')
    cy.get('button.btnChangePassword').click()
    cy.get('div.v-dialog.v-dialog--active').should('be.visible')
    cy.get('input[name=oldPassword]').clear().type('12345678')
    cy.get('input[name=newPassword]').clear().type('12345')
    cy.get('input[name=confirmPassword]').clear().type('12345')
    cy.get('#updatePassword').contains('Save').click({ force: true })
    cy.get('div.error').should('be.visible').contains('Wrong password')
  })
  it('Change password', () => {
    cy.login('admin@admin.com')
    cy.setLocaleToEN()
    cy.visit('/profile')
    cy.get('button.btnChangePassword').click()
    cy.get('div.v-dialog.v-dialog--active').should('be.visible')
    cy.get('input[name=oldPassword]').clear().type('12345')
    cy.get('input[name=newPassword]').clear().type('12345')
    cy.get('input[name=confirmPassword]').clear().type('12345')
    cy.get('#updatePassword').contains('Save').click({ force: true })
    cy.get('div.success')
      .should('be.visible')
      .contains('Password changed successfully')
    // Logout
    cy.logout()
  })
})
