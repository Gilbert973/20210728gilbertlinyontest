import axios from 'axios'
const querystring = require('querystring')

const headers = { 'Content-Type': 'application/x-www-form-urlencoded' }

export default {
  userSignUp(body) {
    return axios.post('/register', querystring.stringify(body), { headers })
  }
}
