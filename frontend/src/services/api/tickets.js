import axios from 'axios'
const querystring = require('querystring')

const headers = { 'Content-Type': 'application/x-www-form-urlencoded' }

export default {
  getTickets(params) {
    return axios.get('/ticket', { params })
  },
  editTicket(id, body) {
    return axios.patch(`/ticket/${id}`, querystring.stringify(body), {
      headers
    })
  },
  saveTicket(body) {
    return axios.post('/ticket/', querystring.stringify(body))
  }
}
