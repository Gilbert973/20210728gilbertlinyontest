import axios from 'axios'
const querystring = require('querystring')

export default {
  resetPassword(body) {
    return axios.post('/password/reset', querystring.stringify(body))
  }
}
