import axios from 'axios'
const querystring = require('querystring')

const headers = { 'Content-Type': 'application/x-www-form-urlencoded' }

export default {
  login(body) {
    return axios.post('/login', querystring.stringify(body), { headers })
  },
  refreshToken() {
    return axios.get('/token')
  }
}
