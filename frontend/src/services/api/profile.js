import axios from 'axios'
const querystring = require('querystring')

export default {
  changeMyPassword(body) {
    return axios.patch('/password/change', querystring.stringify(body))
  },
  getProfile(id) {
    return axios.get(`/user/${id}`)
  },
  saveProfile(id, body) {
    return axios.patch(`/user/${id}`, querystring.stringify(body))
  },
  getActivities(id) {
    return axios.get(`/user/activities/${id}`)
  },
  notDisplayUserActivity(id, type) {
    const uri = type === '1' ? '/user/access' : '/password'
    console.log(type, uri)
    return axios.patch(
      `${uri}/display/${id}`,
      querystring.stringify({ display: 0 })
    )
  }
}
