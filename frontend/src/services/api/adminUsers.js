import axios from 'axios'
const querystring = require('querystring')

const headers = { 'Content-Type': 'application/x-www-form-urlencoded' }

export default {
  getUsers(params) {
    return axios.get('/user', { params })
  },
  editUser(id, body) {
    return axios.patch(`/user/${id}`, querystring.stringify(body), { headers })
  },
  saveUser(body) {
    return axios.post('/user/', querystring.stringify(body))
  },
  deleteUser(id) {
    return axios.delete(`/user/${id}`)
  }
}
