import axios from 'axios'
const querystring = require('querystring')

export default {
  forgotPassword(body) {
    return axios.post('/password/forgot', querystring.stringify(body))
  }
}
