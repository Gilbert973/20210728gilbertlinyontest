import axios from 'axios'
const querystring = require('querystring')

export default {
  sendVerify(body) {
    return axios.post('/verify', querystring.stringify(body))
  }
}
