import * as types from '@/store/mutation-types'
import api from '@/services/api/tickets'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  tickets: (state) => state.tickets,
  totalTickets: (state) => state.totalTickets
}

const actions = {
  getTickets({ commit }, payload) {
    console.log(payload)
    return new Promise((resolve, reject) => {
      api
        .getTickets(payload)
        .then((response) => {
          if (response.status === 200) {
            commit(types.TICKETS, response.data.docs)
            commit(types.TOTAL_TICKETS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editTicket({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = { ...payload }
      api
        .editTicket(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess({ msg: 'common.SAVED_SUCCESSFULLY' }, commit, resolve)
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveTicket({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .saveTicket(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess({ msg: 'common.SAVED_SUCCESSFULLY' }, commit, resolve)
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.TICKETS](state, tickets) {
    state.tickets = tickets
  },
  [types.TOTAL_TICKETS](state, value) {
    state.totalTickets = value
  }
}

const state = {
  tickets: [],
  totalTickets: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
