import * as types from '@/store/mutation-types'
import api from '@/services/api/profile'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  profile: (state) => state.profile,
  activities: (state) => state.activities
}

const actions = {
  changeMyPassword({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true)
      api
        .changeMyPassword(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess({ msg: 'myProfile.PASSWORD_CHANGED' }, commit, resolve)
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  getProfile({ commit }) {
    const user = JSON.parse(localStorage.getItem('user'))
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true)
      api
        .getProfile(user._id)
        .then((response) => {
          if (response.status === 200) {
            commit(types.FILL_PROFILE, response.data)
            buildSuccess(null, commit, resolve)
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  getActivities({ commit }) {
    const id = JSON.parse(localStorage.getItem('user'))._id
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true)
      api
        .getActivities(id)
        .then((response) => {
          if (response.status === 200) {
            commit(types.ACTIVITIES, response.data)
            buildSuccess(null, commit, resolve)
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  notDisplayUserActivity({ commit }, data) {
    console.log(data)
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true)
      api
        .notDisplayUserActivity(data.id, data.type)
        .then((response) => {
          if (response.status === 200) {
            commit(types.ACTIVITY_REMOVED, response.data)
            buildSuccess({ msg: 'myProfile.ACTIVITY_REMOVED' }, commit, resolve)
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveProfile({ commit }, payload) {
    const id = JSON.parse(localStorage.getItem('user'))._id
    payload._id = id
    return new Promise((resolve, reject) => {
      commit(types.SHOW_LOADING, true)
      api
        .saveProfile(id, { ...payload })
        .then((response) => {
          if (response.status === 200) {
            commit(types.FILL_PROFILE, response.data)
            buildSuccess(
              { msg: 'myProfile.PROFILE_SAVED_SUCCESSFULLY' },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  addProfileData({ commit }, data) {
    commit(types.ADD_PROFILE_DATA, data)
  }
}

const mutations = {
  [types.FILL_PROFILE](state, data) {
    state.profile.verified = data.verified
    state.profile.firstName = data.firstName
    state.profile.lastName = data.lastName
    state.profile.email = data.email
    state.profile.phone = data.phone
  },
  [types.ADD_PROFILE_DATA](state, data) {
    switch (data.key) {
      case 'firstName':
        state.profile.firstName = data.value
        break
      case 'lastName':
        state.profile.lastName = data.value
        break
      case 'phone':
        state.profile.phone = data.value
        break
      default:
        break
    }
  },
  [types.ACTIVITIES](state, data) {
    state.activities = data
  },
  [types.ACTIVITY_REMOVED](state, data) {
    state.activities.find((activity, index) => {
      console.log(activity.id)
      if (activity.id === data._id) {
        console.log(data.id)
        state.activities[index].display = false
      }
    })
  }
}

const state = {
  profile: {
    verified: false,
    firstName: '',
    lastName: '',
    email: '',
    phone: ''
  },
  activities: []
}

export default {
  state,
  getters,
  actions,
  mutations
}
