export default [
  {
    path: '/user',
    name: 'user',
    meta: { requiresAuth: true },
    component: () =>
      import(/* webpackChunkName: "profile" */ '@/components/Profile.vue')
  }
]
