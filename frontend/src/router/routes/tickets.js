export default [
  {
    path: '/tickets',
    name: 'tickets',
    component: () =>
      import(/* webpackChunkName: "about" */ '@/components/Tickets.vue')
  }
]
